/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "Audio.h"

Audio::Audio()
{

    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    
    //load the filePlayerS into the mixerAudioSource
    
    for (int i = 0; i < NumFilePlayers; i++)
    {
        mixerAudioSource.addInputSource(&filePlayers[i], false);
    }
    
    audioSourcePlayer.setSource(&mixerAudioSource);

    
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
    
    amplitude = 1;
    
    
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
    audioSourcePlayer.setSource (nullptr);
    mixerAudioSource.removeAllInputs();
}

FilePlayer* Audio::getFilePlayer(int choiceOfPlayer)

{
    if (choiceOfPlayer < NumFilePlayers)
    {
        return &filePlayers[choiceOfPlayer];
    }
    else
    {
        return &filePlayers[0];
    }
    
}


void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    //All MIDI inputs arrive here
}

void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    // get the audio from our file player - player puts samples in the output buffer
    audioSourcePlayer.audioDeviceIOCallback (inputChannelData, numInputChannels, outputChannelData, numOutputChannels, numSamples);
    
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    float inSampL;
    float inSampR;
    
    while(numSamples--)
    {
        inSampL = *outL;
        inSampR = *outL;
        
        *outL = inSampL * amplitude.get();
        *outR = inSampR * amplitude.get();
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    audioSourcePlayer.audioDeviceAboutToStart (device);
}

void Audio::audioDeviceStopped()
{
    audioSourcePlayer.audioDeviceStopped();
}

void Audio::setAmplitude (float newAmplitude)

{
    amplitude = newAmplitude;
}

