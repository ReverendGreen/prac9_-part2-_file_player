/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent (Audio& audio_) : audio (audio_)
{
    
    // Because of the multiple File Players/File Player Guis I cannot initialiser them in the constructor initialiser list so I have to call the filePlayerGui.setFilePlayer() function and pass it the pointer to the correct FilePlayer using the Audio object's .getFilePlayer() method, which returns a pointer to one of it's fileplayers depending on the argument you give it.
    for (int i = 0; i < Audio::NumFilePlayers; i++)
    {
        filePlayerGuis[i].setFilePlayer (audio.getFilePlayer(i));
        addAndMakeVisible(filePlayerGuis[i]);
    }

   
    
    gainSlider.setRange(0.0, 1.0);
    gainSlider.setValue(1.0);
    addAndMakeVisible(&gainSlider);
    gainSlider.addListener(this);

    
    setSize (500, 400);
}

MainComponent::~MainComponent()
{

}

void MainComponent::resized()
{
    Rectangle<int> r (getLocalBounds());
    
    for (int i = 0; i < Audio::NumFilePlayers; i++)
        
    {
        filePlayerGuis[i].setBounds (r.removeFromTop(100));
        r.removeFromTop(25);
    }
    
    
    
    gainSlider.setBounds (r.removeFromTop (50));
}

 void MainComponent::sliderValueChanged (Slider* slider)

{
    if (slider == &gainSlider)
        
    {
        audio.setAmplitude(gainSlider.getValue());
    }
}

//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}

