/*
  ==============================================================================

    FilePlayerGui.h
    Created: 22 Jan 2013 2:49:07pm
    Author:  tj3-mitchell

  ==============================================================================
*/

#ifndef H_FILEPLAYERGUI
#define H_FILEPLAYERGUI

#include "../JuceLibraryCode/JuceHeader.h"
#include "FilePlayer.h"

/**
 Gui for the FilePlayer class
 */
class FilePlayerGui :   public Component,
                        public Button::Listener,
                        public FilenameComponentListener,
                        public Slider::Listener,
                        public Timer

{
public:
    
    FilePlayerGui();
    /**
     constructor - receives a reference to a FilePlayer object to control
     */
    FilePlayerGui (FilePlayer* filePlayer_);
    
    /**
     Destructor 
     */
    ~FilePlayerGui();
    
    
    /** Including a method to set the FilePlayer within the current GUI as initialisng an Array with a constructor with arguments (such as the previous constructor that required a reference to a (FilePlayer& filePlayer_)) is more difficult so is easier to set here and call this method WITHIN a parent class constructor
     */
    void setFilePlayer (FilePlayer* filePlayer_);
    
    //Component
    void resized()override;
    
    //Button Listener
    void buttonClicked (Button* button)override;
    
    //FilenameComponentListener
    void filenameComponentChanged (FilenameComponent* fileComponentThatHasChanged)override;
    
    void sliderValueChanged (Slider* slider) override;
    
    void timerCallback()override;

private:
    TextButton playButton;
    FilenameComponent* fileChooser;
    
    // filePlayer changed from expecting a reference 'FilePlayer&' to a pointer 'FilePlayer*'
    FilePlayer* filePlayer;
    Slider posSlider;
    Slider pitchSlider;
    Slider gainSlider;
    
    
    
};


#endif  // H_FILEPLAYERGUI
