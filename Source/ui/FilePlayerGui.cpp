/*
  ==============================================================================

    FilePlayerGui.cpp
    Created: 22 Jan 2013 2:49:07pm
    Author:  tj3-mitchell

  ==============================================================================
*/

#include "FilePlayerGui.h"

FilePlayerGui::FilePlayerGui() : filePlayer (nullptr)
{
    playButton.setButtonText (">");
    playButton.addListener(this);
    addAndMakeVisible(&playButton);
    
    posSlider.setRange(0.0, 1.0);
    addAndMakeVisible(&posSlider);
    posSlider.addListener(this);
    
    pitchSlider.setRange(0.01, 5.0);
    pitchSlider.setValue(1.0);
    addAndMakeVisible(&pitchSlider);
    pitchSlider.addListener(this);
    
    gainSlider.setRange(0.0, 1.0);
    gainSlider.setValue(1.0);
    addAndMakeVisible(&gainSlider);
    gainSlider.addListener(this);
    gainSlider.setColour(Slider::backgroundColourId, Colours::cadetblue);
    
    AudioFormatManager formatManager;
    formatManager.registerBasicFormats();
    fileChooser = new FilenameComponent ("audiofile",
                                         File::nonexistent,
                                         true, false, false,
                                         formatManager.getWildcardForAllFormats(),
                                         String::empty,
                                         "(choose a WAV or AIFF file)");
    fileChooser->addListener(this);
    addAndMakeVisible(fileChooser);
    
}

FilePlayerGui::FilePlayerGui (FilePlayer* filePlayer_) : filePlayer (filePlayer_)
{
    playButton.setButtonText (">");
    playButton.addListener(this);
    addAndMakeVisible(&playButton);
    
    posSlider.setRange(0.0, 1.0);
    addAndMakeVisible(&posSlider);
    posSlider.addListener(this);
    
    pitchSlider.setRange(0.01, 5.0);
    pitchSlider.setValue(1.0);
    addAndMakeVisible(&pitchSlider);
    pitchSlider.addListener(this);
    
    AudioFormatManager formatManager;
    formatManager.registerBasicFormats();
    fileChooser = new FilenameComponent ("audiofile",
                                         File::nonexistent,
                                         true, false, false,
                                         formatManager.getWildcardForAllFormats(),
                                         String::empty,
                                         "(choose a WAV or AIFF file)");
    fileChooser->addListener(this);
    addAndMakeVisible(fileChooser);
    
}

void FilePlayerGui::setFilePlayer (FilePlayer* filePlayer_)
{
    filePlayer = filePlayer_;
}

FilePlayerGui::~FilePlayerGui()
{
    delete fileChooser;
}


//Component
void FilePlayerGui::resized()
{
    Rectangle<int> r = (getLocalBounds());
    
    playButton.setBounds (0, 0, getHeight() / 2, getHeight() / 4);
    fileChooser->setBounds (getHeight(), 0, getWidth()-getHeight(), getHeight() / 4);
    posSlider.setBounds(0, getHeight()/4, getWidth(), getHeight() / 4);
    pitchSlider.setBounds(0, (getHeight()/4) * 2, getWidth(), getHeight() / 4);
    gainSlider.setBounds(0, (getHeight()/4) * 3, getWidth(), getHeight() / 4);
    
    
    
}

//Button Listener
void FilePlayerGui::buttonClicked (Button* button)
{
    if (button == &playButton)
    {
        filePlayer->setPlaying(!filePlayer->isPlaying());
        
        filePlayer->isPlaying() ? startTimer(250) : stopTimer();
    }
}

//FilenameComponentListener
void FilePlayerGui::filenameComponentChanged (FilenameComponent* fileComponentThatHasChanged)
{
    if (fileComponentThatHasChanged == fileChooser)
    {
        File audioFile (fileChooser->getCurrentFile().getFullPathName());
        
        if(audioFile.existsAsFile())
        {
                filePlayer->loadFile(audioFile);
        }
        else
        {
            AlertWindow::showMessageBox (AlertWindow::WarningIcon,
                                         "sdaTransport",
                                         "Couldn't open file!\n\n");
        }
    }
}

void FilePlayerGui::sliderValueChanged (Slider* slider)

{
    if (slider == &posSlider)
    
    {
        filePlayer->setPosition(posSlider.getValue());
    }
    
    else if (slider == &pitchSlider)
        
    {
        filePlayer->setPlaybackRate(pitchSlider.getValue());
    }
    
     else if (slider == &gainSlider)
         
     {
         filePlayer->setGain(gainSlider.getValue());
     }
}

void FilePlayerGui::timerCallback()

{
    posSlider.setValue(filePlayer->getPosition());
}
